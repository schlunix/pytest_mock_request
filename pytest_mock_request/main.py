import requests


def get_user(user_id):
    # get the user from the API
    """
    Args:
        user_id:
    """
    url = 'https://jsonplaceholder.typicode.com/todos/'
    response = requests.get('{}{}'.format(url, user_id))

    # make sure the response is good
    if response.ok and (response.status_code == 200):
        # response looks like
        # {
        #   "userId": 1,
        #   "id": 1,
        #   "title": "delectus aut autem",
        #   "completed": false
        # }

        # return the json payload
        return response.json()
    else:
        return None


def get_multiple_users(list_users):
    """
    Args:
        list_users:
    """
    if type(list_users) is not list:
        raise TypeError("'list_users' must be of type list")

    list_result = []
    for user in list_users:
        list_result.append(get_user(user))
