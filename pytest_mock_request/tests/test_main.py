import pytest

import pytest_mock_request.main


class TestClassGetUser:
    @staticmethod
    @pytest.fixture
    def get_user_response():
        def response(user_id):
            # mimic the response of the real API which looks like:
            # {
            #   "userId": 1,
            #   "id": 1,
            #   "title": "delectus aut autem",
            #   "completed": "false"
            # }
            dict_responses = {
                1: {
                    "userId": 1,
                    "id": 1,
                    "title": "delectus aut autem",
                    "completed": "false"
                },
                2: {
                    "userId": 1,
                    "id": 2,
                    "title": "quis ut nam facilis et officia qui",
                    "completed": "false"
                },
                3: {
                    "userId": 1,
                    "id": 3,
                    "title": "fugiat veniam minus",
                    "completed": "false"
                }
            }

            return dict_responses[user_id]

        return response

    @staticmethod
    @pytest.mark.parametrize(
        "params,expected",
        [
            (
                1,
                {
                    "userId": 1,
                    "id": 1,
                    "title": "delectus aut autem",
                    "completed": "false"
                }
            ),
            (
                2,
                {
                    "userId": 1,
                    "id": 2,
                    "title": "quis ut nam facilis et officia qui",
                    "completed": "false"
                }
            ),
            (
                3,
                {
                    "userId": 1,
                    "id": 3,
                    "title": "fugiat veniam minus",
                    "completed": "false"
                }
            )
        ]
    )
    def test_mocked_get_user(params, expected, requests_mock, get_user_response):
        # use requests_mock for the actual path that will be requested in requests.get in main.get_user()
        """
        Args:
            params:
            expected:
            requests_mock:
            get_user_response:
        """
        requests_mock.get(
            'https://jsonplaceholder.typicode.com/todos/{}'.format(params),
            json=get_user_response(params)
        )

        assert expected == pytest_mock_request.main.get_user(params)

    @staticmethod
    @pytest.mark.parametrize(
        "params,expected",
        [
            (
                4,
                {
                    "userId": 1,
                    "id": 4,
                    "title": "et porro tempora",
                    "completed": True
                }
            ),
            (
                5,
                {
                    "userId": 1,
                    "id": 5,
                    "title": "laboriosam mollitia et enim quasi adipisci quia provident illum",
                    "completed": False
                }
            ),
            (
                6,
                {
                    "userId": 1,
                    "id": 6,
                    "title": "qui ullam ratione quibusdam voluptatem quia omnis",
                    "completed": False
                }
            )
        ]
    )
    def test_api_get_user(params, expected):
        # not mocking here, really making the call through the API
        """
        Args:
            params:
            expected:
        """
        assert expected == pytest_mock_request.main.get_user(params)
