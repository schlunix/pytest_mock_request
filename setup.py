"""A setuptools based setup module.
See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

# prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path, environ

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name=environ['CI_PROJECT_NAME'],  # Required
    version=environ['CI_COMMIT_TAG'],  # Required
    url=environ['CI_PROJECT_URL'],  # Optional
    description=environ['SETUP_PROJECT_DESCRIPTION'],  # Required

    author=environ['SETUP_PROJECT_AUTHOR'],  # Optional
    author_email=environ['SETUP_PROJECT_AUTHOR_EMAIL'],  # Optional
    classifiers=[
        environ['SETUP_CLASSIFIER_DEV'],
        environ['SETUP_CLASSIFIER_AUDIENCE'],
        environ['SETUP_CLASSIFIER_TOPIC'],
        environ['SETUP_CLASSIFIER_LICENSE'],  # License :: OSI Approved :: Apache Software License
        'Programming Language :: Python',
    ],
    keywords=environ['SETUP_PROJECT_KEYWORDS'],
    packages=find_packages(),
    install_requires=[
        # pytest is always required because tests are built into the application
        'pytest',
        'requests',
        'requests-mock'
    ],
    include_package_data=True,
)
